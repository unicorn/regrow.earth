---
title: Source Code, Licenses and More
date: 2023-09-09
unlisted: true
aliases:
- /source.html
draft: true
---

All content of the site is licensed under Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). The code used to generate and present the web version of the site is published under the MIT license.

## What does this mean?
You can adapt the articles on this site, as long as you give appropriate credit, link to the license and indicate whether changes where made. If you make changes or build on the content, you also have to distribute your contributions under the same license. 
=> https://creativecommons.org/licenses/by-sa/4.0/	Full CC-BY-SA 4.0 license text

Besides the articles, there is also code involved in this site, such as HTML and CSS files. These, along with all the gemtext files that the site is originally made of, can be found here:
=> https://codeberg.org/unicorn/regrow.earth		Regrow.Earth source code

The site's HTML version is generated from the gemtext version by a script called "gem2html" written by sloum and myself, Edin Taric (unicorn). The source code for the script is available here:
=> https://codeberg.org/unicorn/gem2html		gem2html source code


=> /index.gmi	Back to the home page
