---
title: About Regrow.Earth
date: 2023-08-21
aliases:
- /about.html
unlisted: true
---

## Source Code, Licenses and more
{{< copyright >}}

## How is the site being run?
This site is served by a dedicated server running Alpine Linux, hosted at [Hetzner](https://www.hetzner.com/), powered by hydroelectric power. It is currently only served via HTTPS  due to a rework but may in the future be served via [Gemini](http://geminiquickst.art/) again.
