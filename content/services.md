---
title: Regrow.Earth Services
date: 2023-09-09
draft: true
unlisted: true
---

Regrow.Earth provides various digital services, such as a cloud for file storage, sharing, as well as contact and calendar synchronization.

## How does it work?
If you are curious about the services but not ready to commit, you can create a free one month trial account at the link below to get an overview and test the features we provide. If you like what you see, you can continue by choosing a paid plan that best suits your needs.
=> account	Create a trial account

## Plans
* Basic: 50GB Storage - 5€ monthly or 50€ yearly
* Small: 100GB Storage - 10€ monthly or 100€ yearly
* Medium: 250GB Storage - 20€ monthly or 200€ yearly
* Large: 500GB Storage - 30€ monthly or 300€ yearly
* Max: 1000GB Storage - 40€ monthly or 400€ yearly

To start your plan, send an email to services@regrow.earth with your chosen plan and (if you used a trial account) the email address you used for your trial account to services@regrow.earth. Your account will then be created/upgraded and you will receive payment instructions. Currently we support PayPal or direct bank transfers, please let us know if you would like to use a different payment method so we can consider adding it.

All of our plans offer the same features. If you have other needs, such as more storage, or are interested in other services, we are happy to figure out a solution that matches your needs.

Once you have started your paid plan, we provide a free hour-long onboarding session to introduce you to our services and help you with setting up your devices.

## Are there any free plans or discounts?
Anybody can sign up and try our service at no cost for the first month, after which you can choose one of our paid plans.
Our services are paid because we want to be accountable only to our users. We are proud to be free of investors and we do not rely on other means for making profits that would force us to make decisions against your best interest. Accordingly, our users' needs and requests are the driving force behind our services. We believe that this is important to building a truly sustainable business, so at this time it is not possible for us to openly provide a free plan.

If you cannot affort a plan but have a need for it, you can send us an email and we will try to figure something out that works for you. :)

## What is your cancellation policy?
You can cancel at any time before the next billing cycle. There is no minimum duration or notice period, we know that there are many reasons why one might not want to or might not be able to pay for a service, so we want to be fair to you and allow you to stop at any time.

If you are using our services and happen to be tight on money but wish to continue, as mentioned above, please just let us know and we can figure something out that works for you.

## Why Regrow.Earth?
As we all known, there are many cloud services, many of which are even offered "for free" by tech-giants such as Google and Microsoft. Especially with "free" services, you usually end up paying with your data, as these companies make a significant amount of their profit by analyzing user data and selling data or targeted advertising opportunities to advertisers. Instead of being a customer, you end up being the product.

We believe that this careless mass-collection of data and profiling is a threat to our fundamental rights, and countless leaks of highly personal data that was collected by large tech companies such as Facebook would prove us right. Data collection should be minimized and user data should be treated with utmost care, which  was our main reason for starting these services as a sustainable, privacy-respecting and human-centric alternative to the well-known giants.

All of our services are based on free and open-source software that is being directly managed by us. If you have requests or suggestions for further services to add, we are happy to expand the range of services we provide unless there are significant reasons against it.

## Who is behind Regrow.Earth?
Regrow.Earth is run by me, Edin Taric. I am currently a student of technical computer science and working at my university's datacenter. Before working with servers professionally and studying computer science, I have been using and providing my own digital services to family for several years. Regrow.Earth's services came to life with a friend wanting to use my cloud. The regulatory difficulty involved and her positive feedback prompted me to set up this small business to be able to provide paid sustainable and privacy-respecting services to users outside of my family.

