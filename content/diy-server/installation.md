---
title: Alpine Installation
date: 2021-11-12
aliases:
- /diy-server/installation.html
authors:
- Edin Tarić
---

This guide will walk you through installing Alpine Linux on a server or regular computer. I will guide you through getting the installation image, booting it up and then finally the installation procedure itself.

If you are looking to use Alpine Linux on an ARM single board computer (SBC) like a Raspberry Pi, this procedure likely won't work or will be missing some important parts. I intend to cover those in a separate guide at a later date. So for this guide, I will be assuming an x86 computer, which includes all Intel and AMD CPUs from the last 2 decades. You should however check whether you have a 32-bit or 64-bit CPU because our choice of an installation image will depend on it. Any AMD or Intel CPU since 2003 or 2004 should be a 64-bit CPU, but you can check this list to be sure:

[List of x86 CPUs - Simple English Wikipedia](https://simple.wikipedia.org/wiki/X86#List_of_x86_CPUs)

## Getting Alpine Linux
Now that you know whether you have a 32 or 64-bit CPU, it is time for us to go download the Alpine Linux installation image. Go to the [Alpine Downloads page](https://alpinelinux.org/downloads/) and pick the right image for your CPU from the "Standard" images. In the case of a 32-bit CPU, you'd pick the "x86" image; for 64-bit you pick "x86_64". The "Standard" image of Alpine Linux is pretty small but includes everything you need for a simple server.

Note: If you want to install Alpine with the ZFS filesystem for your root partition, you will need to pick the "Extended" image here, which includes some additional packages and kernel modules, including the ones necessary for ZFS. If ZFS means nothing to you yet, please ignore this for now and go ahead with the "Standard" image, as I may make a separate guide for a more advanced setup with ZFS. :)

### Verifying the ISO's integrity
Along with the installation image you picked, we want to download the corresponding SHA256 file, which contains a checksum that allows you to verify integrity of the installation image. This can quickly tell us if the file got corrupted while downloading, which can happen. In a terminal, navigate to the directory where you downloaded both files and run the following command to check the ISO's integrity:
``` shell command sha256sum
sha256sum -c alpine-standard-3.14.3-x86_64.iso.sha256
```

This command uses "sha256sum" Of course you will need to replace the filename from my example with the name of the SHA256 file that you downloaded. in my case, the output looks like this:
``` shell command output
$ sha256sum -c alpine-standard-3.14.3-x86_64.iso.sha256 
alpine-standard-3.14.3-x86_64.iso: OK
```

If you see "No such file or directory", it means the file could not be found and you should make sure they are both in the same directory from which you are running the command. If you see "checksum did NOT match", the file is corrupt and you should download it again.


## Preparing a bootable USB stick
To run Alpine on the computer, we first have to write the installation image to some kind of installation medium that the computer can boot from. I will illustrate one way of doing this using the command-line tool "dd" to write the image to a USB drive. There are also graphical tools for other operating systems, but since I cannot cover it all in this guide, I recommend you to look up "how to create a bootable USB" if you do not have access to a Linux system.

Now, before we can write anything to the USB drive, we have to figure out what it's called in the system. The easiest way is with the command "lsblk", which (put simply) lists all of the storage devices and their partitions. If you do not have this command, you may be able to install it or alternatively use "fdisk -l".

From the size of the devices, you should be able to quickly determine which one is your USB stick. In my case I am using an 8GB stick, and you can quickly spot it in the command output of "lsblk":
``` shell command output of "lsblk"
$ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda           8:0    0 223.6G  0 disk  
├─sda1        8:1    0   498M  0 part  /boot/efi
├─sda2        8:2    0     4G  0 part  /recovery
├─sda3        8:3    0 215.1G  0 part  
│ └─...
└─sda4        8:4    0     4G  0 part  
  └─...
sdb           8:16   0   2.7T  0 disk  
└─sdb1        8:17   0   2.7T  0 part  /mnt/3TB_WDRed
sdc           8:32   0 931.5G  0 disk  
sdd           8:48   1   7.6G  0 disk  
├─sdd1        8:49   1   2.7G  0 part  /media/unicorn/Ubuntu 20.04.2.0 LTS amd64
├─sdd2        8:50   1   3.9M  0 part  
└─sdd3        8:51   1   4.9G  0 part  /media/unicorn/writable
```
You can spot 4 storage devices in this example, you can recognize them from the "TYPE" column, where it says "disk". The four devices are called "sda", "sdb", "sdc", and "sdd" in my case. For you, these will be different, so do not copy paste anything here, you will very likely lose data.

If you pay attention to the "SIZE" column, you will see that one of the devices has a size of "7.6G", which has to be our USB stick! So note down its name, in my case I will go ahead with "sdd", but again, for you this will be different, DO NOT copy this from the guide or you might overwrite the wrong device and lose data! Make sure you have selected the correct device!

Now we can go ahead with writing the image to our USB stick. The "dd" utility will need us to put in a few parameters, the most important are of course the input file, shortened to "if" and the output file, shortened to "of". Our output "file" will be the USB stick, so since my USB stick was "sdd", I will set the output file to "/dev/sdd":
``` shell dd command
$ sudo dd if=alpine-standard-3.14.3-x86_64.iso of=/dev/sdd bs=1M && sudo sync
```

Replace the "if=..." and "of=..." with the location of your input file and the name of your USB stick. Make sure you include the "sudo sync" part at the end to ensure that all the data is written to the USB drive before you detach it. Once these commands are finished, you can safely unplug your bootable USB stick with Alpine Linux!

## Booting Alpine
In case this is your first time booting an operating system from a USB stick, I will give you some basic instructions that should get you running. If you encounter issues, a search engine is your best bet, there are probably many more detailed guides about this topic.

The basics are: Shut down your computer and plug in the USB stick. When you start up your computer, you might first see an image of the motherboard manufacturer and some keys that show up on screen. If you do see something, look for "Boot Menu" or alternatively "BIOS Setup" or something along those lines. Even if you do not see anything, try presing F12 a bunch of times when you turn it on, alternatively try F1 or the delete button. This should get you into a menu where you should also be able to select a device to boot from. If that does not work, please look up instructions for your computer or operating systems. I know from experience that on some devices, these menus are hidden or locked by the manufacturer or operating system by default. It's an unfortunate anti-consumer practice by major manufacturers and operating systems.

Once you are in the boot menu, you can select the USB stick to boot from. This should bring you to the next section of this guide. If you see the USB stick but cannot boot it, check if there is a setting in your BIOS like "UEFI boot only" or "BIOS boot only" and see if changing it can fix your situation.


## Installation procedure
When you first boot up Alpine Linux, you will get a prompt asking you to log in. simply type "root" and hit enter:
``` shell Alpine Linux login prompt
Welcome to Alpine Linux 3.15
Kernel 5.15.4-0-lts on an x86_64 (/dev/tty1)

alpine login: root
```

You should then be logged into the machine and see a welcome message like this:
``` shell Alpine Linux welcome message
Welcome to Alpine!

The Alpine Wiki contains a large amount of how-to guides and general
information about administrating Alpine systems.
See <http://wiki.alpinelinux.org/>.

You can setup the system with the command: setup-alpine

You may change this message by editing /etc/motd.

alpine:~#
```

As this message already tells us, we can start setting up the system with the command "setup-alpine":
``` shell "setup-alpine" command
alpine:~# setup-alpine
```

During the setup procedure, you will see a bunch of prompts that allow you to configure your system. You may also see something in brackets at the end of the prompt, like this:
``` shell "Enter system hostname" prompt
Enter system hostname (short form, e.g. 'foo') [alpine] 
```

The part in brackets is the default. If you don't type anything here and just press enter, it will use whatever is in the brackets, in this case it would use "alpine" as the hostname.


### Keyboard layout
First, Alpine lets us choose a keyboard layout. You should see a list of country codes. For my German keyboard, I type "de" here. Then you will likely be presented with a list of variants of that keyboard layout. Perhaps you can figure out what is special about them by their name or search the web if you are very confused. I usually use the "de-nodeadkeys" variant, so this is what I pick here. If you are having a hard time picking one, you could just go with the default variant that usually has the same name as the country code, which in this case would be "de" again. After you pick your variant, Alpine be configured for your keyboard layout.


### Hostname
Now we get to picking our hostname:
``` shell prompt
Enter system hostname (short form, e.g. 'foo') [localhost] 
```
You can give your system a nickname like "terminator", or you could name it after its functionality, for example "cloud", or after whichever subdomain it will end up serving, like "www". For my mailserver for example, I just picked the hostname "mail" here. For the server that runs this website and the services I provide, I chose "ichigo". Just pick whatever works for you here, and if you really hate it, you can still change it at any time later!

### Networking
After setting the hostname, we are given the option to initialize our network interface(s). My server only has one lan port (eth0) and no wifi, so for me, it only shows this:
``` shell
Available interfaces are: eth0.
Enter '?' for help on bridges, bonding and vlans.
Which one do you want to initialize? (or '?' or 'done') [eth0]
```

I will just press enter here, since it already picked the "eth0" interface as default. Next, we are asked about the IP address for this interface:
``` shell
Which one do you want to initialize? (or '?' or 'done') [eth0] 
Ip address for eth0? (or 'dhcp', 'none', '?') [dhcp] 
```
Here, we have the option of either inserting a static IP address or going with DHCP, which will try to determine our IP address automatically from a DHCP server on the same network. Your home router and most hosting companies have such a DHCP server, so if we select DHCP here, our system should be able to automatically get an IP address from the DHCP server and connect to the internet. I may write a separate guide on configuring static IP addresses, in which I will explain this in more detail. If DHCP does not work for you here and my guide is not yet released, try asking your internet service provider or hosting company for the right details.

For this guide, I will just go with DHCP to get us running quickly. We will also leave any manual network configuration for another guide, so we can answer that prompt with "n" too:
``` shell
Ip address for eth0? (or 'dhcp', 'none', '?') [dhcp] 
Do you want to do any manual network configuration? (y/n) [n] 
udhcpc: started, v1.32.1
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending select for 192.168.1.165
udhcpc: lease of 192.168.1.165 obtained, lease time 43200
```
Here you can see the DHCP client sending a discover signal and receiving the IP address from the DHCP server, which in this case is my home router.

### Setting the root password
Now we are prompted to set the password for the root account:
``` shell
Changing password for root
New password: 
Retype password: 
passwd: password for root changed by root
```
Do not be surprised that you don't see anything you are typing; it is intentionally not shown in plain text on the screen. Type your password, press enter and type it again. This should be a good one. We will disable the root account completely in our next guide, but until then our system should be secure.

### Setting your timezone
Now we are prompted to select our timezone:
``` shell
Which timezone are you in? ('?' for list) [UTC] ?
```
You can first type "?" to see a list of timezones and regions. Since I live in Europe, I will type "Europe/" here:
``` shell
Which timezone are you in? ('?' for list) [UTC] Europe/
What sub-timezone of 'Europe/' are you in? ('?' for list)
```
Now we can list the options with "?" again, in my case the correct one is "Berlin", since I live in Germany. Just pick whichever timezone is correct for you.
``` shell
What sub-timezone of 'Europe/' are you in? ('?' for list) Berlin
```
Now our timezone is set correctly.

### Proxy and NTP
Now we are asked for a HTTP/FTP proxy URL. I am not using one, so I will just leave it at "none":
``` shell
HTTP/FTP proxy URL? (e.g. 'http://proxy:8080', or 'none') [none] 
```

Now we are asked for which NTP client to run:
``` shell
Which NTP client to run? ('busybox', 'openntpd', 'chrony' or 'none') [chrony]
```
To simplify, you could say the choice between "chrony" and "openntpd" is just between how accurate you need things to be. According to what I have read, chrony is more accurate, but it is also more complex and big. OpenNTPD is less accurate, but also simpler and smaller. For a server, it's probably better to choose Chrony. For a laptop or desktop computer that is used casually, OpenNTPD should be good enough and thus a good choice. Since we are setting up a server, let's go with "chrony" here, which is the default.

### APK mirrors
Alpine Package Keeper (apk) is the package manager of Alpine Linux. We will use it primarily to install all the software in future guides. In this step, we are asked to pick a mirror that will be used by apk to download packages. You are pretty free to pick any of them. Perhaps you will recognize a university or other institution from your country here, this may be a way for you to pick something that you trust. If you are very uncertain, you can go with number 1 or let Alpine pick a mirror for you randomly or by checking which is fastest.
You can find all available mirrors here:

[Alpine Linux mirrors](https://mirrors.alpinelinux.org/)

I will pick the mirror of a German university that I know, which is number 15 in the list at the time of writing:
``` shell
[...]
60) mirrors.bfsu.edu.cn
61) ftpmirror2.infania.net
62) repo.int.ac.ir

r) Add random from the above list
f) Detect and add fastest mirror from above list
e) Edit /etc/apk/repositories with text editor

Enter mirror number (1-62) or URL to add (or r/f/e/done) [1] 15
Added mirror ftp.halifax.rwth-aachen.de
Updating repository indexes... done.
```

### SSH
SSH will allow us to log in to our server remotely. We are prompted for which SSH server to choose here, given a choice between OpenSSH and Dropbear. Dropbear is very tiny and minimal, which may be useful for small embedded systems for example. For our purposes we will pick OpenSSH though, which is the most widespread and featureful SSH server. In the next guide, we will also go over how to harden its configuration for improved security.
``` shell
Which SSH server? ('openssh', 'dropbear' or 'none') [openssh]
```
Then, the system will generate our SSH host keys and start the SSH server.

### Installing to disk
Now we will be prompted to choose a disk to install the system to. If you want to install Alpine to a ZFS partition, press CTRL+C at this point to cancel the process and refer to this guide for the rest of the installation: (guide does not exist yet, please refer to the alpine wiki)

For everybody else you will be looking at something like this:
``` shell
Available disks are:
  sda    (21.5 GB QEMU    QEMU HARDDISK    )
Which disk(s) would you like to use? (or '?' for help or 'none') [none]
```
Here you need to choose the name of the disk that you want to install to, in my case that is "sda", for you this may be different.
Make sure you type the correct disk here because the data on that disk will be erased!

Next we need to choose how to use that disk. In this guide, we are doing a simple unencrypted system installation. You can learn about the other modes by answering "?" and choose a different installation, such as crypt, lvm or lvmsys.
For this guide, I will proceed with "sys":
``` shell
The following disk is selected:
  sda    (21.5 GB QEMU    QEMU HARDDISK    )
How would you like to use it? ('sys', 'data', 'crypt', 'lvm' or '?' for help) [?] sys
```

Now you will need to confirm that the disk should be erased:
``` shell
WARNING: The following disk(s) will be erased:
  sda    (21.5 GB QEMU    QEMU HARDDISK    )
WARNING: Erase the above disk(s) and continue? (y/n) [n] y
```

Now Alpine Linux will be installed to the selected storage device. If all went well, you will see:
``` shell
Installation is complete. Please reboot.
```

At this point you can restart your computer by typing "reboot" , detach your installation USB drive and your system should boot into Alpine Linux!

## Next steps
Now you can log into the account "root" with the password you previously configured. You now have a running Alpine Linux system that you can use as a basis for anything from your personal computer to a server.

At this point things are not quite optimal yet though, for example we are still using the "root" account, which should not be used in your day to day activities for security reasons, as it has control over everything in the system. You may also want to configure a firewall and harden your system with a few other tweaks. In the next articles in the DIY-Server series, we go through all these fundamentals to understand and configure a secure server system:

* [How to find and read documentation]({{< ref "documentation" >}})
* [First steps for securing your server]({{< ref "first-steps" >}})
* [Nftables Basics: A Simple Firewall]({{< ref "nftables-01-basics" >}})
* [All articles in the DIY-Server series]({{< ref "/diy-server/" >}})
