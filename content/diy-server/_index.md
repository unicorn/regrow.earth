---
title: "DIY Server"
date: 2022-03-01
---
This series of guides covers the entire process of setting up a server with Alpine Linux, starting from the very beginning with installing the operating system and covering any topics that come to my mind or are suggested by others.
