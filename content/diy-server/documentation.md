---
title: "Finding and Reading Linux Documentation"
date: 2021-04-16
lastmod: 2022-08-09
aliases:
- /diy-server/documentation.html
authors:
- Edin Tarić
---

Alpine, being a very compact Linux system, does not include the "man" command by default. Neither does it automatically add the manpages (manuals/documentation) of any other applications you install. To make learning about new commands more easy for you, we will install them before we do anything else so you can read up on how the commands work that you will encounter in this and future guides. Make it a habit to consult the manpages before using search engines, you will learn a lot!

## Installing mandoc tools
The two commands that will be most useful for learning are "man" and "apropos". "man" lets you open a specific manpage and "apropos" lets you search for manpages. We will first install these two tools, which will give us all we need to find and read the manpages.

If you have not done so yet since installing Alpine Linux, let's first make sure our package list and packages are up to date. The first command below updates the list of available packages, the second command upgrades the packages that have a newer version available. We will do this first to update our system before installing anything else.
``` shell commands
# apk update
# apk upgrade
```

Then we install the "mandoc" and "mandoc-apropos" packages that provide the "man" and "apropos" commands that I mentioned above:
``` shell command
# apk add mandoc mandoc-apropos
```

## Installing manpages
This alone won't get us far, since we are still lacking all the actual manpages. In Alpine Linux, the manpages of an application usually come in a separate package that has the same name, except with "-doc" at the end. The manpages for "mandoc" are in the package "mandoc-doc", the manpages for "openrc" are in "openrc-doc" and so on. It's not always the case though and not all packages have manpages associated with them.

For now, let's install the standard linux manpages, as well as the manpages for some important system utilities like "openrc", "apk" and "ssh". I'll also add the manpages of "mandoc" which includes a manual on using the manual!
``` shell command
# apk add man-pages mandoc-doc apk-tools-doc openrc-doc openssh-doc
```

Alternatively, if you want absolutely every manpage of every package you install, there is a package called "docs" that automatically pulls these in when you install a new package. That way, if you install the package "vim" for example, it pulls in "vim-doc" as an automatic dependency without you having to explicitly install it.


## Finding manpages with "apropos"
If I wanted to learn about the "openssh" package, I would first write "apropos openssh" to find any manpages related to it. 
``` shell command and its output
$ apropos openssh
scp(1) - OpenSSH secure file copy
sftp(1) - OpenSSH secure file transfer
ssh(1) - OpenSSH remote login client
ssh-add(1) - adds private key identities to the OpenSSH authentication agent
ssh-agent(1) - OpenSSH authentication agent
ssh-keygen(1) - OpenSSH authentication key utility
ssh_config(5) - OpenSSH client configuration file
sshd_config(5) - OpenSSH daemon configuration file
sftp-server(8) - OpenSSH SFTP server subsystem
ssh-keysign(8) - OpenSSH helper for host-based authentication
ssh-pkcs11-helper(8) - OpenSSH helper for PKCS#11 support
ssh-sk-helper(8) - OpenSSH helper for FIDO authenticator support
sshd(8) - OpenSSH daemon
```

As you can see, we have quite a list of man-pages that apropos found because they contain "OpenSSH" in their description. You also see a number next to the name of the manpage, which tells us which section the manpage is located in. "ssh(1)" means that the "ssh" manpage is located in section 1 of all the manpages.

Section 1 contains manpages about general commands, while section 5 for example contains manpages about file formats. These won't be terribly important unless there are manpages with the same name in two different sections, in which case it's good to note whichever section you are talking about, for example by writing "ssh(1)" instead of just "ssh". You can find more information about sections here:

[Wikipedia article "man page"](https://en.wikipedia.org/wiki/Man_page#Manual_sections)

### Narrowing down "apropos" results
Depending on your search term, you can get a LOT of results that might not be very useful to you. If we had written "apropos ssh" instead of "apropos openssh" earlier, we would have had a 25 lines long and very confusing list that includes lots of things that are unrelated to what we are looking for. We don't always have the option of choosing a more specific search term though.

Let's say we want to learn about using OpenRC to manage the daemons that are running on our server. If we just run "apropos openrc", the output will include a whole bunch of C functions that are irrelevant to us and would be missing several relevant manpages that don't contain "openrc" in their description or name, but just "rc". If we type "apropos rc", we get 147 results, which does contain everything we are looking for but will not be of any use to us either because it also includes so many irrelevant results.

Remember how I mentioned that there are sections that contain manpages of a certain type? Well, section 8 contains "System maintenance and operation commands", according to the "man" manpage. That's exactly what we are looking for, so we will use the "-s" option to only display results from section 8:
``` shell command and output
$ apropos -s 8 rc
openrc(8) - stops and starts services for the specified runlevel
openrc-init(8) - the parent of all processes
openrc-run(8) - a means of hooking shell commands into a service
openrc-shutdown(8) - bring the system down
rc-service(8) - locate and run an OpenRC service with the given arguments
rc-sstat(8) - show status info about services supervised by s6 then rc-status info
rc-status(8) - show status info about runlevels
rc-update(8) - add and remove services to and from a runlevel
```

And there we go! Those are all the manpages we were interested in, and nothing more. Looking for all configuration file formats related to "ssh"? The "man" manpage tells us that "File formats" are covered in section 5, so this is our command:
``` shell command and output
$ apropos -s 5 ssh
ssh_config(5) - OpenSSH client configuration file
sshd_config(5) - OpenSSH daemon configuration file
```
That's it!

There are a lot more options than just restricting the search to a certain section. Apropos lets you input an expression as a search term. What if we wanted to see only the OpenRC administration commands that start with "rc"? We can use the "^" character, which indicates the start. So searching for "^rc" will bring up every manpage that has a name or description starting with "rc". Let's modify our earlier command with this:
``` shell command and output
$ apropos -s 8 ^rc
rc-service(8) - locate and run an OpenRC service with the given arguments
rc-sstat(8) - show status info about services supervised by s6 then rc-status info
rc-status(8) - show status info about runlevels
rc-update(8) - add and remove services to and from a runlevel
```
There we go, we kicked out the other commands that don't start with "rc"!


## Reading manpages with "man"
The "man" command lets us read the manpages, so if I want to learn about the "ssh" command, I would type this to read it:
``` shell command
$ man ssh
```

If there are two manpages with the same name in two different sections, we can specify the section like this:
``` shell command
$ man 1 ssh
```
Now you are presented with the full manpage.

### Using "less" instead of "more"
By default, Alpine Linux uses the application "more" to read the manpages, which I personally don't like that much as it is very limited. So next, we'll install "less", which is a bit more featureful and my personal preference for reading manpages and many other things.

First, we'll need to install the application. I choose to also install the manpages:
``` shell command
# apk add less less-doc
```

Now when we use "man", we will see that we have a different interface. If this is not the case for you, you will have to manually set the "PAGER" environment variable to "less", like this:
``` shell command
# export PAGER=less
```



### Basic controls
You can find an overview of the controls by executing "less --help" or by reading the manpage (heh), but I will summarize the very basics here:
* Go up or down a line using arrow keys or "k" and "j"
* Go down a full page by pressing the space bar
* Search for something by pressing "/", typing your search term and pressing enter. Jump to the next result by pressing "n"
* Show the help page by pressing "h"
* Quit by pressing "q"


## Understanding manpages
When you first open a big manpage, you might be overwhelmed and confused. Don't let this discourage you! There is a lot of pretty dense and technical information on display; in fact it can still be confusing to me when I read about something I have never used before. So let's start with a simple one to understand the general structure!

(unfinished, sorry!)
