---
title: Alpine on the Olimex A20 OLinuXino Lime2 - Short Version
date: 2022-12-09
lastmod: 2023-01-10
authors:
- Edin Tarić
aliases:
- /blog/2022-12-08_alpine-a20-olinuxino-lime2-short.html
unlisted: true
---

## Update 2023-01
Since this whole process is long, arduous and repetitive, I have written a script that allows you to do this fully automatically, either with pre-supplied options or interactively:
[Codeberg.org - alpine-a20-olinuxino-lime2](https://codeberg.org/unicorn/alpine-a20-olinuxino-lime2)

## Preparation
Wipe the start of your uSD card:
``` shell command
$ doas dd if=/dev/zero of=$sdcard bs=1 count=1M
```

Create empty MBR/dos disklabel:
``` shell command
$ echo "label: dos" | doas sfdisk $sdcard
```

### Single partition
To create a single bootable Ext4 partition, we use the following command:
``` shell command
$ echo "type=83, bootable" | doas sfdisk --wipe-partitions always $sdcard
```
"type=83" means that the partition type is "Linux", which is used for ext4

### Multiple partitions
Example creating partition 1 that is 4GB in size, partition 2 that takes the rest of the space:
``` shell command
$ printf "size=4GiB, type=83, bootable\ntype=83" | doas sfdisk --wipe-partitions always $sdcard
```
For each partition you want to create, you pass one line of input to sfdisk, so you can create more by adding more lines

For more details about all possible options, please refer to the sfdisk(8) manpage.


### Creating the filesystems
``` shell command
$ doas mkfs.ext4 $partition
```

## U-Boot
### Installing dependencies:
For Alpine:
``` shell command
$ doas apk add alpine-sdk bc bison dtc flex gcc-arm-none-eabi linux-headers \
ncurses-dev openssl-dev perl python3 py3-setuptools python3-dev sdl2-dev swig 
```

Instructions for other OS:
[u-boot.readthedocs.io - Build U-Boot](https://u-boot.readthedocs.io/en/latest/build/index.html)

### Getting U-Boot:
``` shell commands
$ git clone https://source.denx.de/u-boot/u-boot.git
$ cd u-boot
$ git checkout v2022.10
```

### Configuring U-Boot:
For the eMMC version of the board, edit configs/A20-OLinuXino-Lime2-eMMC_defconfig
For non-eMMC version of the board, edit configs/A20-OLinuXino-Lime2_defconfig

For board revisions A-E:
* add "CONFIG_RTL8211X_PHY_FORCE_MASTER=y"

For board revisions F-G2:
* add "CONFIG_GMAC_TX_DELAY=2"

For board revisions H and later:
* add "CONFIG_PHY_MICREL=y"
* add "CONFIG_PHY_MICREL_KSZ90X1=y"
* add "CONFIG_GMAC_TX_DELAY=4"

### Compiling U-Boot
After we are done editing the defconfig file, we have to run one of these two commands depending on whether or not you have eMMC:
``` shell commands
$ make A20-OLinuXino-Lime2_defconfig      # without eMMC
$ make A20-OLinuXino-Lime2-eMMC_defconfig # with eMMC
```

Finally, we run 'make' to build u-boot:
``` shell command
CROSS_COMPILE=arm-none-eabi- make
```

### Flashing U-Boot
``` shell command
$ doas dd if=u-boot-sunxi-with-spl.bin of=$sdcard bs=1024 seek=8
```

## Alpine Linux
Download alpine and its hash, then verify it:
``` shell commands
$ wget https://dl-cdn.alpinelinux.org/alpine/v3.17/releases/armv7/alpine-uboot-3.17.0-armv7.tar.gz
$ wget https://dl-cdn.alpinelinux.org/alpine/v3.17/releases/armv7/alpine-uboot-3.17.0-armv7.tar.gz.sha256
$ sha256sum -c alpine-uboot-3.17.0-armv7.tar.gz.sha256
```

Mount the bootable partition we created:
``` shell commands
$ mkdir mnt
$ doas mount $partition mnt/
```

Extract alpine archive onto the partition:
``` shell command
$ doas tar -xzf alpine-uboot-3.17.0-armv7.tar.gz -C mnt/
```

Copy device tree binary to the partition:
``` shell command
$ doas cp u-boot/u-boot.dtb mnt/boot/
```

Edit the file "mnt/extlinux/extlinux.conf" to make sure it uses our "u-boot.dtb" file instead of the default:
``` conf extlinux.conf contents
TIMEOUT 10
PROMPT 1
DEFAULT lts

LABEL lts
MENU LABEL Linux lts
KERNEL /boot/vmlinuz-lts
INITRD /boot/initramfs-lts
FDT /boot/u-boot.dtb
APPEND modules=loop,squashfs,sd-mod,usb-storage quiet 
```

Sync and unmount the partition:
``` shell commands
$ doas sync
$ doas umount mnt/
```

Now you can boot and install alpine. You can see the board's output either via serial or the HDMI port. From there onwards you can follow the official installation documentation:
[wiki.alpinelinux.org - Installation](https://wiki.alpinelinux.org/wiki/Installation)

## Conclusion
If you have any trouble or any other questions or suggestions, feel free to send me an email to contact@regrow.earth!

