---
title: Mass Effect 2 with Mods on Linux
date: 2022-05-30
authors:
- Edin Tarić
aliases:
- /blog/2022-05-30_me2_modded_on_linux.html
---

This guide will explain the full process of installing and modding Mass Effect 2 (Origin version, original game, not Legendary Edition) on a Linux computer. First I will explain the setup, but you can skip to the "Installing Lutris" section to get right into it.

Windows applications can be made to run on Linux using Wine, which is a kind of translation layer that makes programs think they are running on Windows while translating all the low-level stuff for your Linux computer to understand. It is different from a virtual machine in that it doesn't actually run a copy of Windows, but just provides the features necessary for Windows applications to run on Linux. In this article I will mention the term "Wine Prefix". This basically means the directory, in which the Windows things get installed, for example "~/Games/mass-effect-2/".

Since managing Wine manually can be a bit difficult, there is a wonderful program called Lutris, which makes managing different Wine environments and applications a lot more convenient and provides many community contributed installers to install Windows applications and games automatically. Such an installer also exists for the Mass Effect games, however we are on our own when it comes to modding, so my guide will explain that in detail. Luckily, Lutris makes this easier for us too.

## Installing Lutris
I recommend that you install Lutris from the package manager/software center of whichever Linux Distribution you are using. As I am using Pop!\_OS as a game system, I simply installed it from the built in software center. 

## Installing Mass Effect 2
Once we have Lutris, we can open it, press on the plus button in the top left and search for Mass Effect 2. Click on the game, then select the Origin version and press Install. As the note says, this will only install Origin and some dependencies, not yet the game itself. Once Origin is done installing, you can launch it and install Mass Effect 2 (and any DLC you want/have) from inside it.

## Configuring Origin
Before we can mod and enjoy the game, we have to change a few settings inside of Origin that can get in the way of modding or playing the game. In the top bar, click on "Origin" and then "Application Settings". Turn off "Automatic game updates", "Automatically update Origin", "Automatically start Origin", "Origin Helper service" and the Origin in-game overlay.
We don't want Origin to do much because it has a tendency to remove all the mods if it tries to update the application. Disabling the in-game overlay can improve performance. You may also want to disable Origin's telemetry and other online features for privacy reasons.

## Launching ME2 for the first time
Now you can launch Mass Effect 2 for the first time. This is a necessary step for it to validate all the downloaded DLC, so make sure you do this once before modding the game.

This initially worked perfectly for me but then I was suddenly unable to start the game from within Origin. If this happens to you, try the following workaround:

In Lutris, open the game's configuration and change the executable path to "OriginGames/Mass Effect 2/Binaries/ME2Game.exe", which will launch the game directly without Origin. Close the game and Origin properly and then try to launch again. If it still does not work, you may be missing some dependencies. Check out these two links for possible solutions:  
[Lutris notes about Origin](https://github.com/lutris/docs/blob/master/Origin.md)  
[Lutris notes about Wine dependencies](https://github.com/lutris/docs/blob/master/WineDependencies.md)  

Other guides can be found on the same GitHub repository or on the Lutris.net website.

## Modding ME2
Modding in Mass Effect today happens mainly through two tools. Firstly, there is ME3Tweaks Mod Manager, also called M3, which handles content mods such as Project Variety. Only rarely are mods installed manually today and M3 is the suggested method. Secondly, there is the ALOT installer (using MassEffectModder in the background), which handles all the texture mods, such as ALOT, MEUITM and any custom textures you may want to install.

For Mass Effect, we always need to install content mods first and texture mods last, otherwise it might create conflicts between different mods. Accordingly, in this guide, I am installing the following mods in the following order:

* [No Mini Games (M3)](https://www.nexusmods.com/masseffect2/mods/63)
* [No Shared Cooldowns patch for No Mini Games (MANUAL)](https://www.nexusmods.com/masseffect2/mods/205)
* [Vignette Remover (M3)](https://www.nexusmods.com/masseffect2/mods/148)
* [Expanded Shepard Armory (M3)](https://www.nexusmods.com/masseffect2/mods/291)
* [Early Recruitment (M3)](https://www.nexusmods.com/masseffect2/mods/384)
* [MEUITM alternative/fixed meshes, links in description (M3)](https://www.nexusmods.com/masseffect2/mods/331)
* [Project Variety (M3)](https://www.nexusmods.com/masseffect2/mods/385)
* [ALOT + Updates + Improved Static Lighting (ALOT)](https://www.nexusmods.com/masseffect2/mods/68)
* [ALOV (ALOT)](https://www.nexusmods.com/masseffect2/mods/245)
* [MEUITM2 (ALOT)](https://www.nexusmods.com/masseffect2/mods/331)
* Femshep/Broshep textures, linked in ALOT installer (ALOT)
* [No Headgear for DLC Armors (ALOT)](https://www.nexusmods.com/masseffect2/mods/376)
* [Asari remaster, excluding Aria (ALOT)](https://www.nexusmods.com/masseffect2/mods/204)

There is a lot to think about in terms of order and compatibility here, so I have done the hard work for you and will be going through these step by step. But before we can start installing mods, we need to install our tools.

### Installing ME3Tweaks Mod Manager
ME3Tweaks Mod Manager is a fantastic open source tool that makes installing content mods much easier. Download the .exe file from the official website:  
[ME3Tweaks Mod Manager](https://me3tweaks.com/modmanager/)

Once it is downloaded, open Lutris, select your Mass Effect 2 installation, click on the little wine glass at the bottom and click on "Run EXE inside Wine prefix". Select the file you downloaded and it should install itself to the Documents folder inside of the Wine prefix. 

Once it has installed itself, you can right click on Mass Effect 2 and select Duplicate. Then right click on the duplicate and click configure. Rename the copy to something else for clarity, for example "ME2 Mod Manager". In the Game Settings, change the executable to the path to the newly installed ME3Tweaks Mod Manager, under "users/yourusername/Documents/ME3TweaksModManager/ME3TweaksModManager.exe" inside the Wine prefix. The Wine prefix location can be found in the Game settings tab too. Finally, in the Runner options, disable DXVK and VKD3D since they seem to cause graphical issues in this application.

Now, you can save and launch the mod manager from within Lutris and it should open up its main window. It will probably display a message that your operating system is not supported, this can safely be ignored.

### Making backups
The mod manager will prompt you to create backups for the game, which I also highly recommend as it can speed things up massively if something goes wrong and you have to revert the game to its initial state. Select a backup location and give it some time to do the backup. It will then recognize the application as backed up and restorable.

### Installing the ALOT installer
There are two options for installing the ALOT installer, depending on how much storage space you have available. Ideally you want the game to be running from an SSD drive for the best loading speeds, but SSDs can be quite small compared to HDDs, so you may not want to keep the tens of gigabytes of texture mod files on the same drive. If you have a small SSD like me, you want to keep the ALOT installer somewhere outside of the Wine prefix on a different drive, which I will explain soon. If you do have enough space, you can do the following:

Inside of the mod manager, click on "Tools" in the top menu bar and then "ALOT Installer", which will install the ALOT installer inside of the Wine prefix. You can also use this to open the ALOT installer in the future, so if you are using this method, disregard my other instructions for installing and opening the ALOT installer.

Now, for those who are less gifted with SSD storage space, like me, you can use this method. First, download the [ALOT installer from the NexusMods page](https://www.nexusmods.com/masseffect2/mods/68?tab=files).

I highly recommend you to use the Torrent as it will be significantly faster than the regular download. Once it is downloaded, place the downloaded folder to your hard drive of choice where all the texture mods will be, and then we can use the same method that we used for installing M3. First, inside of Lutris, right click on ME2 Mod Manager and duplicate it. Then, rename the duplicate to something like "ME2 ALOT" and in the Game settings, set the executable path to the .exe inside the folder we just downloaded. Now launch it and you will see that ALOT opens up. It may also say your operating system is not supported, but again, you can disregard this. It should correctly recognize that Mass Effect 2 is installed and that you have made a backup already. Regarding importing all the texture mods, we will return to this later, since we first need to take care of all content mods.

### Installing the content mods
As mentioned earlier, I want to install the following content mods:
* [No Mini Games (M3)](https://www.nexusmods.com/masseffect2/mods/63)
* [Remove Shared Cooldowns patch for No Mini Games (MANUAL)](https://www.nexusmods.com/masseffect2/mods/205)
* [Vignette Remover (M3)](https://www.nexusmods.com/masseffect2/mods/148)
* [Expanded Shepard Armory (M3)](https://www.nexusmods.com/masseffect2/mods/291)
* [Early Recruitment (M3)](https://www.nexusmods.com/masseffect2/mods/384)
* [MEUITM alternative/fixed meshes, links in description (M3)](https://www.nexusmods.com/masseffect2/mods/331)
* [Project Variety (M3)](https://www.nexusmods.com/masseffect2/mods/385)

Besides the second one, all of them can be installed by M3, the ME3Tweaks Mod Manager. First, Download all the mods and import them into M3 if possible:
* For "No Mini Games", simply download it from NexusMods and import the whole thing into M3. It will automatically recognize the mod.
* For the "Remove Shared Cooldowns patch", select the correct version depending on whether or not you use the controller mod. I will not be installing the controller mod and cannot tell you anything about its compatibility with other mods here, so you will have to read the mod pages and adjust the procedure yourself.
* For "Vignette Remover", "Expanded Shepard Armory", "Early Recruitment" and "Project Variety", simply download their .7z file from the NexusMods "Files" tab and import the whole 7z file into M3.
* For the MEUITM meshes, search the description of the MEUITM page to get all the links, then download them from their respective pages and import them just like the previous mods.

Now they should all be imported and we can start installing them.

First, apply "No Mini Games" and make sure you select the appropriate version depending on whether or not you use the Controller mod. Then, copy the SFXGame.pcc file from "Remove Shared Cooldowns patch" and place it into "drive\_c/Program Files (x86)/Origin Games/Mass Effect 2/BioGame/CookedPC" in your Wine prefix. This is the only time that you manually need to replace a file, this used to be very different in the past, so be happy that you live in the future! :)

Next, we apply the "Vignette Remove", "Expanded Shepard Armory", "Early Recruitment" mods in that order. Then, apply the mesh mods, and then, as the last one, apply the "Project Variety" mod. If you wish to install other mods, do it before Project Variety!

With all of this done, you can launch Mass Effect 2 again (from Lutris, not from M3) to make sure that it still works before we apply the texture mods. Optionally, you can do another backup by simply copying the Wine prefix to a different location.

### Installing the texture mods
Now we move on to installing ALOV and the texture mods with the ALOT installer. This process is quite straightforward, again we start by downloading everything we need. I chose the following:

* [ALOT + Updates + Improved Static Lighting (ALOT)](https://www.nexusmods.com/masseffect2/mods/68)
* [ALOV (ALOT)](https://www.nexusmods.com/masseffect2/mods/245)
* [MEUITM2 (ALOT)](https://www.nexusmods.com/masseffect2/mods/331)
* Femshep/Broshep textures, linked in ALOT installer (ALOT)
* [No Headgear for DLC Armors (ALOT)](https://www.nexusmods.com/masseffect2/mods/376)
* [Asari remaster, excluding Aria (ALOT)](https://www.nexusmods.com/masseffect2/mods/204)

We already downloaded and installed ALOT in the last step (if you chose the method of installing ALOT through M3, you have to download the ALOT files still). Also download any updates for ALOT if there are some, as well as the Improved Static Lighting, also linked on the ALOT page. Again, I highly recommend using torrents as much as possible to speed up the process for yourself and others.

Next, I downloaded ALOV through its torrent, however I only downloaded the 1080p files since I do not have a 4k monitor. I also downloaded MEUITM2 with its torrent link. Next, I downloaded the Femshep/Broshep textures, which are linked in the ALOT installer at the time of writing. Finally, I downloaded the No Headgear for DLC Armors and the Asari remaster, excluding the files for Aria, since I am not sure if it will cause a conflict with the improved Aria mesh that we installed before.

Now we import everything by clicking on "Import Assistant" and then "Select Files to Import" (for ALOT update, static lighting, ALOV, MEUITM2, Femshep/Broshep textures) and "Add Files" for the No Headgear for DLC Armors and the Asari remaster textures. Now they should all be added and listed as green in the installer. For ALOV, of course only the 1080p OR 4k version are required, not both.

Now you can press "Install Textures". You will also be prompted about some options for installing MEUITM2, make sure that you select all the options for the custom meshes we installed. I also recommend selecting the "Crushed Blacks fix" which will light up some too-dark parts of the game. Now you can launch the game, and if you see "ALOT" mentioned on the game's starting screen, the installation has succeeded!

## Conclusion
Congratulations, you have survived the full process of installing and modding this wonderful game, and I hope that you enjoy playing it! Once you have applied all the texture mods, you should not install any content mods over the top of it or you might break the installation. Instead, start over if you want to change something.

If you have any questions, issues or suggestions, feel free to shoot me an email to contact@regrow.earth.

